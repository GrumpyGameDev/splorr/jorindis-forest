local GameApplication = {}
local StateHandler = require("Handler.StateHandler")
local GameImages = require("Game.GameImages")
local GamePalette = require("Game.GamePalette")
local ColorManager = require("Manager.ColorManager")
local ImageManager = require("Manager.ImageManager")

local MainMenuState = require("Game.State.MainMenuState")
local ConfirmQuitState = require("Game.State.ConfirmQuitState")
local HowToPlayState = require("Game.State.HowToPlayState")
local AboutState = require("Game.State.AboutState")
local OptionsState = require("Game.State.OptionsState")
local PlayState = require("Game.State.PlayState")
local PausedState = require("Game.State.PausedState")

local InputFilter = require("Game.InputFilter")

local Direction = require("Common.Direction")

function GameApplication.new()
  love.graphics.setDefaultFilter("nearest","nearest",1)--always do this on load
  
  InputFilter.getKeypressMap()["up"]="up"
  InputFilter.getKeypressMap()["down"]="down"
  InputFilter.getKeypressMap()["left"]="left"
  InputFilter.getKeypressMap()["right"]="right"
  InputFilter.getKeypressMap()["x"]="ok"
  InputFilter.getKeypressMap()["z"]="cancel"
  InputFilter.getKeypressMap()["e"]="interact"
  InputFilter.getKeypressMap()["tab"]="switch"
  InputFilter.getKeypressMap()["f10"]="menu"
  
  InputFilter.getGamepadButtonMap()["dpup"]="up"
  InputFilter.getGamepadButtonMap()["dpdown"]="down"
  InputFilter.getGamepadButtonMap()["dpleft"]="left"
  InputFilter.getGamepadButtonMap()["dpright"]="right"
  InputFilter.getGamepadButtonMap()["a"]="ok"
  InputFilter.getGamepadButtonMap()["b"]="cancel"
  InputFilter.getGamepadButtonMap()["x"]="interact"
  InputFilter.getGamepadButtonMap()["y"]="switch"
  InputFilter.getGamepadButtonMap()["start"]="menu"
  
  local colorManager = ColorManager.new(GamePalette)
  local imageManager = ImageManager.new(GameImages,colorManager)
  
  local instance = StateHandler.new(nil,"mainmenu",
    {
      mainmenu=MainMenuState.new(imageManager),
      howtoplay=HowToPlayState.new(imageManager),
      about=AboutState.new(imageManager),
      options=OptionsState.new(imageManager),
      play=PlayState.new(imageManager),
      paused=PausedState.new(imageManager),
      confirmquit=ConfirmQuitState.new(imageManager)})
  
  return instance
end


return GameApplication