local module = {}
local WeightedGenerator = require "Common.WeightedGenerator"

module.terrain = WeightedGenerator.create()
module.terrain:setWeight("grass",8000)
module.terrain:setWeight("tree",2000)
module.terrain:setWeight("rock",500)
module.terrain:setWeight("flintrock",50)
module.terrain:setWeight("bush",25)
module.terrain:setWeight("rabbithole",10)

return module