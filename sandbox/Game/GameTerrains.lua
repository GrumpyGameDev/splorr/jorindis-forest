function never()
  return false
end
function always()
  return true
end
function doNothing()
end

function makeTerrain(imageName,foregroundColor,backgroundColor)
  return
    {imageName=imageName,
    foregroundColor=foregroundColor,
    backgroundColor=backgroundColor,
    isPassable=never,
    onEnter=doNothing,
    onBump=doNothing,
    onInteract=doNothing}
end

function makeGrassTerrain()
  local instance = makeTerrain("field","darkjade","black")
  instance.isPassable=always
  return instance
end

function makeTreeTerrain()
  local instance = makeTerrain("tree","jade","black")
  function instance:onBump(creature)
  end
  return instance
end



return {
  grass=makeGrassTerrain(),
  bush=makeTerrain("bush","darkjade","black"),
  tree=makeTreeTerrain(),
  rock=makeTerrain("rock","darksilver","black"),
  rabbithole=makeTerrain("hole","copper","black"),
  flintrock=makeTerrain("rock","darkersilver","black"),
  empty=makeTerrain("empty","black","black")
  }