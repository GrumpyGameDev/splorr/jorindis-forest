local MenuState = {}
local EventHandler = require("Handler.EventHandler")
local Menu = require("Menu.Menu")
local MenuItem = require("Menu.MenuItem")
local MenuRenderer = require("Renderer.MenuRenderer")
local RenderGrid = require("Grid.RenderGrid")
local RenderGridCellLayer = require("Grid.RenderGridCellLayer")
local GridRenderer = require("Renderer.GridRenderer")
local StateBase = require("Game.State.StateBase")

function MenuState.new(menu)
  local instance = StateBase.new()
  
  function instance:onCommand(command)
    return false
  end
  
  function instance:onFilteredInput(input)
    if input=="up" then
      menu:previousItem()
      return true
    elseif input=="down" then
      menu:nextItem()
      return true
    elseif input=="ok" then
      return self:onCommand(menu:getSelectedItem():getId())
    else
      return false
    end
  end
  
  return instance
end


return MenuState