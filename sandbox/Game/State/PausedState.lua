local PausedState = {}
local EventHandler = require("Handler.EventHandler")
local MenuState = require("Game.State.MenuState")

local Menu = require("Menu.Menu")
local MenuItem = require("Menu.MenuItem")
local MenuRenderer = require("Renderer.MenuRenderer")

local MapRenderer = require("Renderer.MapRenderer")

local GameData = require("Game.Data.GameData")

function PausedState.new(imageManager)
  local menu = Menu.new(1,
    {MenuItem.new("Resume","resume"),
     MenuItem.new(" Quit ","quit")})
  
  local instance = MenuState.new(menu)
  
  local menuRenderer = MenuRenderer.new(imageManager,menu,"ruby","black",32,48,8,8)
  local mapRenderer = MapRenderer.new(imageManager,nil,0,0,8,8)
  
  function instance:getImageManager()
    return imageManager
  end
  
  function instance:onDraw()
    local player = GameData:getGame():getPlayer()
    local creatureInstance=player:getInstance()
    local mapCell = creatureInstance:getCell()
    
    mapRenderer:setMap(mapCell:getMap())
    
    mapRenderer:render()
    
    menuRenderer:render()
    
    return false
  end
  
  local oldOnFilteredInput = instance.onFilteredInput
  
  function instance:onFilteredInput(input)
    if input=="menu" or input=="cancel" then
      self:getStateMachine():setState("play")
      return true
    else
      return oldOnFilteredInput(self,input)
    end
  end
  
  function instance:onCommand(command)
      if command=="quit" then
        self:getStateMachine():setState("mainmenu")
        return true
      elseif command=="resume" then
        self:getStateMachine():setState("play")
        return true
      else
        return false
      end
  end
  
  return instance
end


return PausedState