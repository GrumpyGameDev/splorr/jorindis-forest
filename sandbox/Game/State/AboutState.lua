local AboutState = {}
local EventHandler = require("Handler.EventHandler")
local Menu = require("Menu.Menu")
local MenuItem = require("Menu.MenuItem")
local MenuRenderer = require("Renderer.MenuRenderer")
local RenderGrid = require("Grid.RenderGrid")
local RenderGridCellLayer = require("Grid.RenderGridCellLayer")
local GridRenderer = require("Renderer.GridRenderer")
local MenuState = require("Game.State.MenuState")

function AboutState.new(imageManager)
  local menu = Menu.new(1,
    {MenuItem.new("Back","back")})
  
  local instance = MenuState.new(menu)
  
  local menuRenderer = MenuRenderer.new(imageManager,menu,"ruby","black",64,14*8,8,8)
  local renderGrid = RenderGrid.new(20,15)
  local gridRenderer = GridRenderer.new(imageManager,renderGrid,0,0,8,8)
  
  renderGrid:fill(1,1,20,15,"empty","black","black",true)
  renderGrid:writeText(1,1,"About","turquoise","black")

  function instance:getImageManager()
    return imageManager
  end
  
  function instance:onDraw()
    
    gridRenderer:render()
    
    menuRenderer:render()
    
    return false
  end
  
  function instance:onCommand(command)
      if command=="back" then
        self:getStateMachine():setState("mainmenu")
        return true
      else
        return false
      end
  end
  
  local oldOnFilteredInput = instance.onFilteredInput
  
  function instance:onFilteredInput(input)
    if input=="cancel" then
      self:getStateMachine():setState("mainmenu")
      return true
    else
      return oldOnFilteredInput(instance,input)
    end
  end
  
  return instance
end


return AboutState