local PlayState = {}
local StateBase=require("Game.State.StateBase")
local MapRenderer=require("Renderer.MapRenderer")
local GameData=require("Game.Data.GameData")

function PlayState.new(imageManager)
  local instance=StateBase.new()
  
  function instance:onFilteredInput(input)
    if input=="left" or input=="right" or input=="up" or input=="down" then
      local player = GameData:getGame():getPlayer()
      local creatureInstance=player:getInstance()
      
      local oldMapCell = creatureInstance:getCell()
      
      local oldMapCellColumn = oldMapCell:getColumn()
      local oldMapCellRow = oldMapCell:getRow()
      local oldMapCellAtlasColumn = oldMapCell:getAtlasColumn():getColumn()
      local oldMapCellAtlasRow = oldMapCell:getMap():getRow()
      
      local newMapCellColumn=oldMapCellColumn
      local newMapCellRow=oldMapCellRow
      local newMapCellAtlasColumn=oldMapCellAtlasColumn
      local newMapCellAtlasRow=oldMapCellAtlasRow
      
      if input=="left" then
        player:setFacing("west")
        newMapCellColumn = newMapCellColumn - 1
      elseif input=="right" then
        player:setFacing("east")
        newMapCellColumn = newMapCellColumn + 1
      elseif input=="up" then
        player:setFacing("north")
        newMapCellRow = newMapCellRow - 1
      elseif input=="down" then
        player:setFacing("south")
        newMapCellRow = newMapCellRow + 1
      end
      
      if newMapCellColumn<1 then
        newMapCellColumn = newMapCellColumn + oldMapCell:getMap():getColumns()
      elseif newMapCellColumn>oldMapCell:getMap():getColumns() then
        newMapCellColumn = newMapCellColumn - oldMapCell:getMap():getColumns()
      elseif newMapCellRow<1 then
        newMapCellRow = newMapCellRow + oldMapCell:getMap():getRows()
      elseif newMapCellRow>oldMapCell:getMap():getRows() then
        newMapCellRow = newMapCellRow - oldMapCell:getMap():getRows()
      end
    
      local newMapCell = GameData:getGame():getAtlas():getMap(newMapCellAtlasColumn,newMapCellAtlasRow):getCell(newMapCellColumn,newMapCellRow)
      local cursorMapCell = newMapCell
      
      if not newMapCell:getTerrain():terrain():isPassable(creatureInstance,newMapCell) then
        if newMapCell:getTerrain():terrain().onBump~=nil then
          print("bump")
          newMapCell:getTerrain():terrain():onBump(creatureInstance)
        end
        newMapCell = oldMapCell
      end
      
      oldMapCell:setCreature(nil)
      newMapCell:setCreature(creatureInstance)
      
      --now move cursor
      local cursor = creatureInstance:getCursor()
      if cursor.getCell~=nil then
        cursor:getCell():setEffect(nil)
      end
      
      cursorMapCell:setEffect(cursor)
    
      return true
    elseif input=="menu" then
      self:getStateMachine():setState("paused")
      return true
    else
      return false
    end
  end
  
  local mapRenderer = MapRenderer.new(imageManager,nil,0,0,8,8)
  
  function instance:onDraw()
    local player = GameData:getGame():getPlayer()
    local creatureInstance=player:getInstance()
    local mapCell = creatureInstance:getCell()
    
    mapRenderer:setMap(mapCell:getMap())
    
    mapRenderer:render()
    
    return false
  end
  
  return instance
end

return PlayState