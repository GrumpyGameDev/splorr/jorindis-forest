local GameConstants = {}

local atlasConstants = {}

function atlasConstants.columns()
  return 1
end

function atlasConstants.rows()
  return 1
end

function GameConstants.atlas()
  return atlasConstants
end
local mapConstants = {}

function mapConstants.columns()
  return 256
end

function mapConstants.rows()
  return 256
end

function GameConstants.map()
  return mapConstants
end

return GameConstants