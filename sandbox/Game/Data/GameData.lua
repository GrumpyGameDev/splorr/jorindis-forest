local CommonMeta = require("Meta.CommonMeta")

local GameData = {}
local GameConstants = require("Game.Data.GameConstants")
local GamePlayer = require("Game.Data.GamePlayer")

local MazeDirections = require("Game.MazeDirections")
local Maze = require("Maze.Maze")
local MazeGenerator = require("Maze.MazeGenerator")

local Atlas = require("Atlas.Atlas")

local MapDirections = require("Game.MapDirections")

local Terrains = require("Game.GameTerrains")
local TerrainManager = require("Manager.TerrainManager")
local Creatures = require("Game.GameCreatures")
local CreatureManager = require("Manager.CreatureManager")
local Effects = require("Game.GameEffects")
local EffectManager = require("Manager.EffectManager")
local Generators = require "Game.GameGenerators"

function GameData.newGame()
  local game = {}
  CommonMeta.makeGetter(GameData,"getGame",game)
  
  local atlas = Atlas.new(GameConstants.atlas().columns(),GameConstants.atlas().rows(),GameConstants.map().columns(),GameConstants.map().rows(),MapDirections)
  CommonMeta.makeGetter(game,"getAtlas",atlas)
  CommonMeta.makeGetter(atlas,"getGame",game)
  
  local player = GamePlayer.new()
  CommonMeta.makeGetter(game,"getPlayer",player)
  CommonMeta.makeGetter(player,"getGame",game)
  
  local terrainManager = TerrainManager.new(Terrains)
  local creatureManager = CreatureManager.new(Creatures)
  local effectManager=EffectManager.new(Effects)
  
  for atlasColumn=1,atlas:getColumns() do
    for atlasRow = 1,atlas:getRows() do
      local map = atlas:getMap(atlasColumn,atlasRow)
      
      for mapColumn=1,map:getColumns() do
        for mapRow=1,map:getRows() do
          
          
          local cell = map:getCell(mapColumn,mapRow)
          cell:setTerrain(terrainManager:createInstance(Generators.terrain:generate()))
        end
      end
    end
  end
      
  local tagonInstance = creatureManager:createInstance("tagon")
  CommonMeta.makeGetter(tagonInstance,"getPlayer",player)
  CommonMeta.makeGetter(player,"getInstance",tagonInstance)
  local tagonCursor = effectManager:createInstance("cursor")
  CommonMeta.makeGetter(tagonInstance,"getCursor",tagonCursor)
  
  
  local mapCell = nil
  repeat
    local atlasX = love.math.random(1,GameConstants.atlas().columns())
    local atlasY = love.math.random(1,GameConstants.atlas().rows())
    local mapX = love.math.random(1,GameConstants.map().columns())
    local mapY = love.math.random(1,GameConstants.map().rows())
    mapCell = atlas:getMap(atlasX,atlasY):getCell(mapX,mapY)
  until mapCell:getTerrain():terrain():isPassable(tagonInstance,mapCell)
  mapCell:setCreature(tagonInstance)
  mapCell:setEffect(tagonInstance:getCursor())
  
  print("done")--this makes it work for some reason, so don't remove it
end

return GameData