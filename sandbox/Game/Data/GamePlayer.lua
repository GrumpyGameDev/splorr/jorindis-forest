local CommonMeta = require("Meta.CommonMeta")
local GamePlayer = {}

function GamePlayer.new()
  local instance = {}
  
  local facing=""
  function instance:getFacing()
    return facing
  end
  function instance:setFacing(newFacing)
    facing = newFacing
  end
  
  return instance
end

return GamePlayer