local FlagDoubler = {}

local doublerTable={
  [0]={{6,3},{12,9}},
  [1]={{5,3},{5,9}},
  [2]={{6,3},{10,10}},
  [3]={{5,3},{3,10}},
  [4]={{6,5},{12,5}},
  [5]={{5,5},{5,5}},
  [6]={{6,5},{10,6}},
  [7]={{5,5},{3,6}},
  [8]={{10,10},{12,9}},
  [9]={{9,10},{5,9}},
  [10]={{10,10},{10,10}},
  [11]={{9,10},{3,10}},
  [12]={{10,12},{12,5}},
  [13]={{9,12},{5,5}},
  [14]={{10,12},{10,6}},
  [15]={{9,12},{3,6}}
}


function FlagDoubler.doubleFlags(flags)
  local columns = #flags
  local rows = #(flags[1])
  
  local result = {}
  while #result<(columns*2) do
    local column={}
    while #column<(rows*2) do
      column[#column+1]=0
    end
    result[#result+1]=column
  end
  
  for column=1,columns do
    for row=1,rows do
      local flag = flags[column][row]
      for x=1,2 do
        for y=1,2 do
          result[column*2+x-2][row*2+y-2]=doublerTable[flag][x][y]
        end
      end
    end
  end
  
  return result
end


return FlagDoubler