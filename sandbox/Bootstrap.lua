local Bootstrap = {}

function Bootstrap.application()
  return "Game.GameApplication"
end

function Bootstrap.debug()
  return false
end

function Bootstrap.predraw()
  love.graphics.push("all")
  love.graphics.scale(1,1)
end

function Bootstrap.postdraw()
  love.graphics.pop()
end

return Bootstrap