local ItemManager = {}
local Item = require("Map.Entity.Item")
local ItemInstance = require("Map.Instance.ItemInstance")

function ItemManager.new(descriptors)
  local instance = {}
  
  local items = {}
  
  function instance:getItem(id)
    if items[id]==nil then
      items[id]=Item.new(descriptors[id])
    end
    return items[id]
  end
  
  function instance:createInstance(id,data)
    return ItemInstance.new(self:getItem(id),data)
  end
  
  return instance
end

return ItemManager