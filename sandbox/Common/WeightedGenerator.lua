local WeightedGenerator = {}

WeightedGenerator.create = function ()
  local generator = {}
  
  local _table={}
  local _total=0
  
  function generator:setWeight(value,weight)
    _total = _total - self:getWeight(value)
    if weight > 0 then
      _table[value]=weight
    else
      _table[value]=nil
    end
    _total = _total + self:getWeight(value)
  end
  
  function generator:getWeight(value)
    if _table[value] == nil then
      return 0
    else
      return _table[value]
    end
  end
  
  function generator:generate()
    local generated = love.math.random(1,_total)
    for k,v in pairs(_table) do
      if generated<=v then
        return k
      else
        generated = generated - v
      end
    end
    assert(false)
  end
  
  function generator:combine(other,combiner)
    local result = WeightedGenerator.create()
    for i1,outer in ipairs(self:values()) do
      for i2,inner in ipairs(other:values()) do
        local combined = combiner(outer,inner)
        result:setWeight(combined,result:getWeight(combined)+self:getWeight(outer) * other:getWeight(inner))
      end
    end
    return result
  end
  
  function generator:values()
    local result={}
    for k,v in pairs(_table) do
      result[#result+1]=k
    end
    return result
  end
  
  return generator
end

return WeightedGenerator