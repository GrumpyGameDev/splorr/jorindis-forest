local Map = {}

local MapColumn = require("Map.MapColumn")

function Map.new(atlasColumn,row,columns,rows,directions)
  local instance = {}
  
  local mapColumns = {}
  
  function instance:getAtlasColumn()
    return atlasColumn
  end
  
  function instance:getAtlas()
    return self:getAtlasColumn():getAtlas()
  end
  
  function instance:getColumns()
    return columns
  end
  
  function instance:getRows()
    return rows
  end
  
  function instance:getRow()
    return row
  end
  
  for column=1,columns do
    mapColumns[column]=MapColumn.new(instance,column,rows)
  end
  
  function instance:getColumn(column)
    return mapColumns[column]
  end
  
  function instance:getCell(column,row)
    if column>=1 and column<=columns and row>=1 and row<=rows then
      return self:getColumn(column):getCell(row)
    else
      return nil
    end
  end
  
  return instance
end

return Map