local Terrain = {}

function Terrain.new(descriptor)
  local instance = {}
  
  for k,v in pairs(descriptor) do
    if type(v)=="function" then
      instance[k]=v
    else
      instance[k]=function(a) return v end
    end
  end
  
  return instance
end

return Terrain