local CreatureInstance = {}

function CreatureInstance.new(creature)
  local instance = {}
  if creature.instance~=nil then
    local descriptor = creature.instance()
    for k,v in pairs(descriptor) do
      if type(v)=="function" then
        instance[k]=v
      else
        instance[k]=function(a) return v end
      end
    end
  end
  function instance:creature()
    return creature
  end
  return instance
end

return CreatureInstance