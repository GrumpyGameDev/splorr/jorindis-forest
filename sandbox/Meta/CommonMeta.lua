local Common = {}

function Common.makeGetter(instance,name,object)
  instance[name] = function(x) return object end
end

return Common