local MapRenderer = {}

function MapRenderer.new(imageManager,map,offsetX,offsetY,columnWidth,rowHeight,scaleX,scaleY)
  local instance = {}
  
  scaleX = scaleX or 1
  scaleY = scaleY or 1
  
  local renderColumns=15--TODO: unhardcode
  local renderRows = 15--TODO: unhardcode
  local offsetColumn=8--TODO: unhardcode
  local offsetRow=8--TODO: unhardcode
  
  function instance:setMap(newMap)
    map = newMap
  end
  
  function instance:render()
    love.graphics.push("all")
    love.graphics.scale(scaleX,scaleY)
    
    local playerInstance = map:getAtlas():getGame():getPlayer():getInstance()
    
    for row=1,renderRows do 
      local sourceRow = row - offsetRow + playerInstance:getCell():getRow()
      if sourceRow<1 then 
        sourceRow = sourceRow + map:getRows()
      elseif sourceRow>map:getRows() then
        sourceRow = sourceRow - map:getRows()
      end
      
      for column=1,renderColumns do
        local sourceColumn = column - offsetColumn + playerInstance:getCell():getColumn()
        if sourceColumn<1 then 
          sourceColumn = sourceColumn + map:getColumns()
        elseif sourceColumn>map:getColumns() then
          sourceColumn = sourceColumn - map:getColumns()
        end
        
        local x, y = column*columnWidth-columnWidth+offsetX,row*rowHeight-rowHeight+offsetY
        local cell = map:getCell(sourceColumn,sourceRow)
        
        local terrain = cell:getTerrain()
        if terrain~= nil then
          local image = imageManager:getImage(terrain:terrain():imageName(),terrain:terrain():foregroundColor(),terrain:terrain():backgroundColor())
          love.graphics.draw(image,x,y)
        end
        
        local creature = cell:getCreature()
        if creature~= nil then
          local image = imageManager:getImage(creature:creature():imageName(),creature:creature():foregroundColor(),creature:creature():backgroundColor())
          love.graphics.draw(image,x,y)
        end
        
        local effect = cell:getEffect()
        if effect~= nil then
          local image = imageManager:getImage(effect:effect():imageName(),effect:effect():foregroundColor(),effect:effect():backgroundColor())
          love.graphics.draw(image,x,y)
        end
        
        local item = cell:getItem()
        if item~= nil then
          local image = imageManager:getImage(item:item():imageName(),item:item():foregroundColor(),item:item():backgroundColor())
          love.graphics.draw(image,x,y)
        end
        
      end
    end
    love.graphics.pop()

  end
  
  return instance
end

return MapRenderer