local GridRenderer = {}

function GridRenderer.new(imageManager,renderGrid,offsetX,offsetY,columnWidth,rowHeight, scaleX,scaleY)
  local instance = {}
  scaleX = scaleX or 1
  scaleY = scaleY or 1
  
  function instance:render()
    love.graphics.push("all")
    love.graphics.scale(scaleX,scaleY)
    for row=1,renderGrid:getRows() do
      for column=1,renderGrid:getColumns() do
        for layer=1,renderGrid:getCell(column,row):getLayers() do
          local cellLayer = renderGrid:getCell(column,row):getLayer(layer)
          love.graphics.draw(imageManager:getImage(cellLayer:getImageName(),
              cellLayer:getForegroundColor(),cellLayer:getBackgroundColor()),column*columnWidth-columnWidth+offsetX,row*rowHeight-rowHeight+offsetY)
        end
      end
    end
    love.graphics.pop()
  end
  
  return instance
end

return GridRenderer