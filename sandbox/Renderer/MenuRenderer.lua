local MenuRenderer = {}

function MenuRenderer.new(imageManager,menu,foregroundColor,backgroundColor,offsetX,offsetY,columnWidth,rowHeight,scaleX,scaleY)
  local instance = {}
  scaleX = scaleX or 1
  scaleY = scaleY or 1
  
  function instance:render()
    love.graphics.push("all")
    love.graphics.scale(scaleX,scaleY)
    for row = 1,menu:getItems() do
      local menuItem = menu:getItem(row)
      for column=1,menuItem:getText():len() do
        local x = (column-1) * columnWidth + offsetX
        local y = (row-1) * rowHeight + offsetY
        local imageName = "character" .. menuItem:getText():byte(column)
        if row==menu:getSelectedIndex() then
          love.graphics.draw(imageManager:getImage(imageName,
            backgroundColor,foregroundColor),x,y)        
        else
          love.graphics.draw(imageManager:getImage(imageName,
            foregroundColor,backgroundColor),x,y)        
        end
      end
    end
    love.graphics.pop()
  end
  
  return instance
end

return MenuRenderer