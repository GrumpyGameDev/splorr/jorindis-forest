local EffectManager = {}
local Effect = require("Map.Entity.Effect")
local EffectInstance = require("Map.Instance.EffectInstance")

function EffectManager.new(descriptors)
  local instance = {}
  
  local effects = {}
  
  function instance:getEffect(id)
    if effects[id]==nil then
      effects[id]=Effect.new(descriptors[id])
    end
    return effects[id]
  end
  
  function instance:createInstance(id,data)
    return EffectInstance.new(self:getEffect(id),data)
  end
  
  return instance
end

return EffectManager