local TerrainManager = {}
local Terrain = require("Map.Entity.Terrain")
local TerrainInstance = require("Map.Instance.TerrainInstance")

function TerrainManager.new(descriptors)
  local instance = {}
  
  local terrains = {}
  
  function instance:getTerrain(id)
    if terrains[id]==nil then
      terrains[id]=Terrain.new(descriptors[id])
    end
    return terrains[id]
  end
  
  function instance:createInstance(id,data)
    return TerrainInstance.new(self:getTerrain(id),data)
  end
  
  return instance
end

return TerrainManager