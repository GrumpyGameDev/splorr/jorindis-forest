local ImageManager = {}

function loadImage(imageBits, columns, colorManager, foregroundColor, backgroundColor)
  local imageData = love.image.newImageData(columns,table.getn(imageBits))
  local y
  local x
  local n
  local r,g,b,a
  for y=0, (table.getn(imageBits)-1) do
    n = imageBits[y+1]
    for x=0,(columns-1) do
      if (n%2)==1 then
        r,g,b,a = colorManager:getRGBA(foregroundColor)
      else
        r,g,b,a = colorManager:getRGBA(backgroundColor)
      end
      imageData:setPixel(x,y,r,g,b,a)
      n = math.floor(n/2)
    end
  end
  return love.graphics.newImage(imageData)
end

local blankImageData = love.image.newImageData(1,1)
blankImageData:setPixel(0,0,0,0,0,0)
local blankImage = love.graphics.newImage(blankImageData)

function ImageManager.new(bitmapTable,colorManager)
  local instance = {}
  instance.colorManager = colorManager
  instance.bitmapTable = bitmapTable
  instance.imageTable = {}
  
  function instance:getImage(imageName,foregroundColor,backgroundColor)
    if instance.imageTable[backgroundColor]~=nil then
      local backgroundTable = instance.imageTable[backgroundColor]
      if backgroundTable[foregroundColor]~=nil then
        local foregroundTable = backgroundTable[foregroundColor]
        if foregroundTable[imageName]~=nil then
          return foregroundTable[imageName]
        else
          if instance.bitmapTable[imageName]~=nil then
            local bitmap = instance.bitmapTable[imageName]
            foregroundTable[imageName] = loadImage(bitmap.data,bitmap.columns,colorManager,foregroundColor,backgroundColor)
            return foregroundTable[imageName]
          else
            foregroundTable[imageName] = blankImage
            return blankImage
          end
        end
      else
        backgroundTable[foregroundColor]={}
        return self:getImage(imageName,foregroundColor,backgroundColor)
      end
    else
      instance.imageTable[backgroundColor]={}
      return self:getImage(imageName,foregroundColor,backgroundColor)
    end
  end
  
  return instance
end

return ImageManager