local CreatureManager = {}
local Creature = require("Map.Entity.Creature")
local CreatureInstance = require("Map.Instance.CreatureInstance")

function CreatureManager.new(descriptors)
  local instance = {}
  
  local creatures = {}
  
  function instance:getCreature(id)
    if creatures[id]==nil then
      creatures[id]=Creature.new(descriptors[id])
    end
    return creatures[id]
  end
  
  function instance:createInstance(id,data)
    return CreatureInstance.new(self:getCreature(id),data)
  end
  
  return instance
end

return CreatureManager