local MenuItem = {}

function MenuItem.new(text,id)
  local instance = {}
  
  function instance:getText()
    return text
  end
  
  function instance:getId()
    return id
  end
  
  return instance
end

return MenuItem