local Cell = {}

function Cell.new(mapColumn,row,terrain,item,creature,effect)
  local instance = {}
  
  function instance:getRow()
    return row
  end
  
  function instance:getAtlasColumn()
    return self:getMap():getAtlasColumn()
  end
  
  function instance:getAtlas()
    return self:getAtlasColumn():getAtlas()
  end
  
  function instance:getColumn()
    return mapColumn:getColumn()
  end
  
  function instance:getMapColumn()
    return mapColumn
  end
  
  function instance:getMap()
    return self:getMapColumn():getMap()
  end
  
  function instance:getTerrain()
    return terrain
  end
  
  function instance:setTerrain(newTerrain)
    if terrain~=nil then
      terrain.getCell = nil
    end
    terrain = newTerrain
    if terrain~=nil then
      function terrain:getCell()
        return instance
      end
    end
  end
  
  function instance:getItem()
    return item
  end
  
  function instance:setItem(newItem)
    if item~=nil then
      item.getCell = nil
    end
    item = newItem
    if item~=nil then
      function item:getCell()
        return instance
      end
    end
  end
  
  function instance:getCreature()
    return creature
  end
  
  function instance:setCreature(newCreature)
    if creature~=nil then
      creature.getCell = nil
    end
    creature = newCreature
    if creature~=nil then
      function creature:getCell()
        return instance
      end
    end
  end
  
  function instance:getEffect()
    return effect
  end
  
  function instance:setEffect(newEffect)
    if effect~=nil then
      effect.getCell = nil
    end
    effect = newEffect
    if effect~=nil then
      function effect:getCell()
        return instance
      end
    end
  end
  
  return instance
end

return Cell