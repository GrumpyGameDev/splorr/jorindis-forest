local EffectInstance = {}

function EffectInstance.new(effect)
  local instance = {}
  if effect.instance~=nil then
    local descriptor = effect.instance()
    for k,v in pairs(descriptor) do
      if type(v)=="function" then
        instance[k]=v
      else
        instance[k]=function(a) return v end
      end
    end
  end
  function instance:effect()
    return effect
  end
  return instance
end

return EffectInstance