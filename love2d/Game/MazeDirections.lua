  local Direction=require("Common.Direction")
  
  return {
    Direction.new("n","e","w","s",function(x,y) return x,y-1 end,0),
    Direction.new("e","s","n","w",function(x,y) return x+1,y end,0),
    Direction.new("s","w","e","n",function(x,y) return x,y+1 end,0),
    Direction.new("w","n","s","e",function(x,y) return x-1,y end,0),
    Direction.new("nne","nee","nnw","ssw",function(x,y) return x+1,y-2 end,0),
    Direction.new("nee","see","nne","sww",function(x,y) return x+2,y-1 end,0),
    Direction.new("see","sse","nee","nww",function(x,y) return x+2,y+1 end,0),
    Direction.new("sse","ssw","see","nnw",function(x,y) return x+1,y+2 end,0),
    Direction.new("ssw","sww","sse","nne",function(x,y) return x-1,y+2 end,0),
    Direction.new("sww","nww","ssw","nee",function(x,y) return x-2,y+1 end,0),
    Direction.new("nww","nnw","sww","see",function(x,y) return x-2,y-1 end,0),
    Direction.new("nnw","nne","nww","sse",function(x,y) return x-2,y-2 end,0)
  }
