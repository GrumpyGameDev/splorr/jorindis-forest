local GameConstants = {}

local mazeConstants = {}

function mazeConstants.columns()
  return 8
end

function mazeConstants.rows()
  return 8
end

function GameConstants.maze()
  return mazeConstants
end
local mapConstants = {}

function mapConstants.columns()
  return 15
end

function mapConstants.rows()
  return 15
end

function mapConstants.horizontalSections()
  return 3
end

function mapConstants.verticalSections()
  return 3
end

function mapConstants.sectionColumns()
  return 5
end

function mapConstants.sectionRows()
  return 5
end

function GameConstants.map()
  return mapConstants
end

return GameConstants