local CommonMeta = require("Meta.CommonMeta")

local GameData = {}
local GameConstants = require("Game.Data.GameConstants")
local GamePlayer = require("Game.Data.GamePlayer")

local MazeDirections = require("Game.MazeDirections")
local Maze = require("Maze.Maze")
local MazeGenerator = require("Maze.MazeGenerator")

local Atlas = require("Atlas.Atlas")

local MapDirections = require("Game.MapDirections")

local Terrains = require("Game.GameTerrains")
local TerrainManager = require("Manager.TerrainManager")
local Creatures = require("Game.GameCreatures")
local CreatureManager = require("Manager.CreatureManager")
local Effects = require("Game.GameEffects")
local EffectManager = require("Manager.EffectManager")

function GameData.newGame()
  local game = {}
  CommonMeta.makeGetter(GameData,"getGame",game)
  
  local maze = Maze.new(GameConstants.maze().columns(),GameConstants.maze().rows(),MazeDirections)
  
  MazeGenerator.generate(maze,MazeDirections)
  
  local atlas = Atlas.new(GameConstants.maze().columns(),GameConstants.maze().rows(),GameConstants.map().columns(),GameConstants.map().rows(),MapDirections)
  CommonMeta.makeGetter(game,"getAtlas",atlas)
  CommonMeta.makeGetter(atlas,"getGame",game)
  
  local player = GamePlayer.new()
  CommonMeta.makeGetter(game,"getPlayer",player)
  CommonMeta.makeGetter(player,"getGame",game)
  
  local terrainManager = TerrainManager.new(Terrains)
  local creatureManager = CreatureManager.new(Creatures)
  local effectManager=EffectManager.new(Effects)
  
  for atlasColumn=1,atlas:getColumns() do
    for atlasRow = 1,atlas:getRows() do
      local map = atlas:getMap(atlasColumn,atlasRow)
      
      for mapColumn=1,map:getColumns() do
        for mapRow=1,map:getRows() do
          local cell = map:getCell(mapColumn,mapRow)
          cell:setTerrain(terrainManager:createInstance("grass"))
        end
      end
      
      local miniMaze = Maze.new(GameConstants.map().horizontalSections(),GameConstants.map().verticalSections(),MapDirections)
      MazeGenerator.generate(miniMaze,MapDirections)
      
      for sectionColumn=1,GameConstants.map().horizontalSections() do
        local sectionOffsetX = (sectionColumn-1) * GameConstants.map().sectionColumns()
        for sectionRow = 1,GameConstants.map().verticalSections() do
          local sectionOffsetY = (sectionRow-1) * GameConstants.map().sectionRows()
          
          local sectionTop = sectionOffsetY+1
          local sectionBottom = sectionOffsetY+GameConstants.map().sectionRows()
          local sectionLeft = sectionOffsetX+1
          local sectionRight = sectionOffsetX+GameConstants.map().sectionColumns()
          
          --four corners
          map:getCell(sectionLeft,sectionTop):setTerrain(terrainManager:createInstance("tree"))
          map:getCell(sectionLeft,sectionBottom):setTerrain(terrainManager:createInstance("tree"))
          map:getCell(sectionRight,sectionTop):setTerrain(terrainManager:createInstance("tree"))
          map:getCell(sectionRight,sectionBottom):setTerrain(terrainManager:createInstance("tree"))
          
          local mazeCell = miniMaze:getCell(sectionColumn,sectionRow)
          
          local doorList = {
            north={x1=sectionLeft+1,x2=sectionRight-1,y1=sectionTop,y2=sectionTop},
            south={x1=sectionLeft+1,x2=sectionRight-1,y1=sectionBottom,y2=sectionBottom},
            east={x1=sectionRight,x2=sectionRight,y1=sectionTop+1,y2=sectionBottom-1},
            west={x1=sectionLeft,x2=sectionLeft,y1=sectionTop+1,y2=sectionBottom-1}
          }
          
          for direction,value in pairs(doorList) do
            
            local door = mazeCell:getDoor(direction)
            if door==nil or not door:isOpen() then
              for x=value.x1,value.x2 do
                for y=value.y1,value.y2 do
                  map:getCell(x,y):setTerrain(terrainManager:createInstance("tree"))
                end
              end
            end
          end
          
          for index=1,2 do
            local x = love.math.random(sectionLeft+1,sectionRight-1)
            local y = love.math.random(sectionTop+1,sectionBottom-1)
            map:getCell(x,y):setTerrain(terrainManager:createInstance("choppabletree"))
          end

        end
      end
      
      local mazeCell = maze:getCell(atlasColumn,atlasRow)
      local door = mazeCell:getDoor("n")
      if door~=nil and door:isOpen() then
        for x=7,9 do
          map:getCell(x,1):setTerrain(terrainManager:createInstance("grass"))
        end
      end
      door = mazeCell:getDoor("nnw")
      if door~=nil and door:isOpen() then
        for x=2,4 do
          map:getCell(x,1):setTerrain(terrainManager:createInstance("grass"))
        end
      end
      door = mazeCell:getDoor("nne")
      if door~=nil and door:isOpen() then
        for x=12,14 do
          map:getCell(x,1):setTerrain(terrainManager:createInstance("grass"))
        end
      end
      
      
      door = mazeCell:getDoor("s")
      if door~=nil and door:isOpen() then
        for x=7,9 do
          map:getCell(x,15):setTerrain(terrainManager:createInstance("grass"))
        end
      end
      door = mazeCell:getDoor("ssw")
      if door~=nil and door:isOpen() then
        for x=2,4 do
          map:getCell(x,15):setTerrain(terrainManager:createInstance("grass"))
        end
      end
      door = mazeCell:getDoor("sse")
      if door~=nil and door:isOpen() then
        for x=12,14 do
          map:getCell(x,15):setTerrain(terrainManager:createInstance("grass"))
        end
      end
      
      door = mazeCell:getDoor("w")
      if door~=nil and door:isOpen() then
        for y=7,9 do
          map:getCell(1,y):setTerrain(terrainManager:createInstance("grass"))
        end
      end
      door = mazeCell:getDoor("nww")
      if door~=nil and door:isOpen() then
        for y=2,4 do
          map:getCell(1,y):setTerrain(terrainManager:createInstance("grass"))
        end
      end
      door = mazeCell:getDoor("sww")
      if door~=nil and door:isOpen() then
        for y=12,14 do
          map:getCell(1,y):setTerrain(terrainManager:createInstance("grass"))
        end
      end
      
      door = mazeCell:getDoor("e")
      if door~=nil and door:isOpen() then
        for y=7,9 do
          map:getCell(15,y):setTerrain(terrainManager:createInstance("grass"))
        end
      end
      door = mazeCell:getDoor("nee")
      if door~=nil and door:isOpen() then
        for y=2,4 do
          map:getCell(15,y):setTerrain(terrainManager:createInstance("grass"))
        end
      end
      door = mazeCell:getDoor("see")
      if door~=nil and door:isOpen() then
        for y=12,14 do
          map:getCell(15,y):setTerrain(terrainManager:createInstance("grass"))
        end
      end
    end
  end
  local tagonInstance = creatureManager:createInstance("tagon")
  CommonMeta.makeGetter(tagonInstance,"getPlayer",player)
  CommonMeta.makeGetter(player,"getInstance",tagonInstance)
  local tagonCursor = effectManager:createInstance("cursor")
  CommonMeta.makeGetter(tagonInstance,"getCursor",tagonCursor)
  
  
  local mapCell = nil
  repeat
    local atlasX = love.math.random(1,GameConstants.maze().columns())
    local atlasY = love.math.random(1,GameConstants.maze().rows())
    local mapX = love.math.random(1,GameConstants.map().columns())
    local mapY = love.math.random(1,GameConstants.map().rows())
    mapCell = atlas:getMap(atlasX,atlasY):getCell(mapX,mapY)
  until mapCell:getTerrain():terrain():isPassable(tagonInstance,mapCell)
  mapCell:setCreature(tagonInstance)
  mapCell:setEffect(tagonInstance:getCursor())
  
  
  print("done")--this makes it work for some reason, so don't remove it
end

return GameData