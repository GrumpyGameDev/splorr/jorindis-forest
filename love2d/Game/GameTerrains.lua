function never()
  return false
end
function always()
  return true
end
function doNothing()
end

return {
  grass={imageName="field",foregroundColor="darkjade",backgroundColor="black",isPassable=always,onEnter=doNothing},
  water={imageName="field",foregroundColor="ruby",backgroundColor="darkruby",isPassable=never,onEnter=doNothing},
  path={imageName="field",foregroundColor="black",backgroundColor="darksilver",isPassable=always,onEnter=doNothing},
  bush={imageName="bush",foregroundColor="darkjade",backgroundColor="black",isPassable=never,onEnter=doNothing},
  tree={imageName="tree",foregroundColor="jade",backgroundColor="black",isPassable=never,onEnter=doNothing},
  choppabletree={imageName="tree",foregroundColor="mediumjade",backgroundColor="black",isPassable=never,onEnter=doNothing},
  house={imageName="house",foregroundColor="carnelian",backgroundColor="black",isPassable=never,onEnter=doNothing},
  empty={imageName="empty",foregroundColor="black",backgroundColor="black",isPassable=always,onEnter=doNothing}
  }