local MainMenuState = {}
local EventHandler = require("Handler.EventHandler")
local MenuState = require("Game.State.MenuState")

local Menu = require("Menu.Menu")
local MenuItem = require("Menu.MenuItem")
local MenuRenderer = require("Renderer.MenuRenderer")

local RenderGrid = require("Grid.RenderGrid")
local GridRenderer = require("Renderer.GridRenderer")

local GameData = require("Game.Data.GameData")

function MainMenuState.new(imageManager)
  local menu = Menu.new(1,
    {MenuItem.new("   Start   ","start"),
     MenuItem.new("How To Play","howtoplay"),
     MenuItem.new("  Options  ","options"),
     MenuItem.new("   About   ","about"),
     MenuItem.new("   Quit    ","quit")})
  
  local instance = MenuState.new(menu)
  
  local menuRenderer = MenuRenderer.new(imageManager,menu,"ruby","black",40,40,8,8)
  local renderGrid = RenderGrid.new(20,15)
  local gridRenderer = GridRenderer.new(imageManager,renderGrid,0,0,8,8)
  
  renderGrid:fill(1,1,20,15,"empty","black","black",true)
  
  function instance:getImageManager()
    return imageManager
  end
  
  function instance:onDraw()
    
    gridRenderer:render()
    
    menuRenderer:render()
    
    return false
  end
  
  function instance:onCommand(command)
      if command=="start" then
        GameData.newGame()
        self:getStateMachine():setState("play")
        return true
      elseif command=="quit" then
        self:getStateMachine():setState("confirmquit")
        return true
      elseif command=="howtoplay" then
        self:getStateMachine():setState("howtoplay")
        return true
      elseif command=="about" then
        self:getStateMachine():setState("about")
        return true
      elseif command=="options" then
        self:getStateMachine():setState("options")
        return true
      else
        return false
      end
  end
  
  return instance
end


return MainMenuState