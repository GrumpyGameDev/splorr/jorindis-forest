local PlayState = {}
local StateBase=require("Game.State.StateBase")
local MapRenderer=require("Renderer.MapRenderer")
local GameData=require("Game.Data.GameData")

function PlayState.new(imageManager)
  local instance=StateBase.new()
  
  function instance:onFilteredInput(input)
    if input=="left" or input=="right" or input=="up" or input=="down" then
      local player = GameData:getGame():getPlayer()
      local creatureInstance=player:getInstance()
      
      local oldMapCell = creatureInstance:getCell()
      
      local oldMapCellColumn = oldMapCell:getColumn()
      local oldMapCellRow = oldMapCell:getRow()
      local oldMapCellAtlasColumn = oldMapCell:getAtlasColumn():getColumn()
      local oldMapCellAtlasRow = oldMapCell:getMap():getRow()
      
      local newMapCellColumn=oldMapCellColumn
      local newMapCellRow=oldMapCellRow
      local newMapCellAtlasColumn=oldMapCellAtlasColumn
      local newMapCellAtlasRow=oldMapCellAtlasRow
      
      if input=="left" then
        newMapCellColumn = newMapCellColumn - 1
      elseif input=="right" then
        newMapCellColumn = newMapCellColumn + 1
      elseif input=="up" then
        newMapCellRow = newMapCellRow - 1
      elseif input=="down" then
        newMapCellRow = newMapCellRow + 1
      end
      
      if newMapCellColumn<1 then
        newMapCellColumn=newMapCellColumn+15
        if newMapCellRow<6 then
          newMapCellAtlasColumn=newMapCellAtlasColumn-2
          newMapCellAtlasRow=newMapCellAtlasRow-1
          newMapCellRow = newMapCellRow + 10
        elseif newMapCellRow>10 then
          newMapCellAtlasColumn=newMapCellAtlasColumn-2
          newMapCellAtlasRow=newMapCellAtlasRow+1
          newMapCellRow = newMapCellRow - 10
        else
          newMapCellAtlasColumn=newMapCellAtlasColumn-1
        end
      elseif newMapCellColumn>15 then
        newMapCellColumn=newMapCellColumn-15
        if newMapCellRow<6 then
          newMapCellAtlasColumn=newMapCellAtlasColumn+2
          newMapCellAtlasRow=newMapCellAtlasRow-1
          newMapCellRow = newMapCellRow + 10
        elseif newMapCellRow>10 then
          newMapCellAtlasColumn=newMapCellAtlasColumn+2
          newMapCellAtlasRow=newMapCellAtlasRow+1
          newMapCellRow = newMapCellRow - 10
        else
          newMapCellAtlasColumn=newMapCellAtlasColumn+1
        end
      elseif newMapCellRow<1 then
        newMapCellRow=newMapCellRow+15
        if newMapCellColumn<6 then
          newMapCellAtlasRow = newMapCellAtlasRow-2
          newMapCellAtlasColumn = newMapCellAtlasColumn - 1
          newMapCellColumn = newMapCellColumn + 10
        elseif  newMapCellColumn>10 then
          newMapCellAtlasRow = newMapCellAtlasRow-2
          newMapCellAtlasColumn = newMapCellAtlasColumn + 1
          newMapCellColumn = newMapCellColumn - 10
        else
          newMapCellAtlasRow = newMapCellAtlasRow-1
        end
      elseif newMapCellRow>15 then
        newMapCellRow=newMapCellRow-15
        if newMapCellColumn<6 then
          newMapCellAtlasRow = newMapCellAtlasRow+2
          newMapCellAtlasColumn = newMapCellAtlasColumn - 1
          newMapCellColumn = newMapCellColumn + 10
        elseif  newMapCellColumn>10 then
          newMapCellAtlasRow = newMapCellAtlasRow+2
          newMapCellAtlasColumn = newMapCellAtlasColumn + 1
          newMapCellColumn = newMapCellColumn - 10
        else
          newMapCellAtlasRow = newMapCellAtlasRow+1
        end
      end
      
    
      local newMapCell = GameData:getGame():getAtlas():getMap(newMapCellAtlasColumn,newMapCellAtlasRow):getCell(newMapCellColumn,newMapCellRow)
      
      if not newMapCell:getTerrain():terrain():isPassable(creatureInstance,newMapCell) then
        newMapCell = oldMapCell
      end
      
      oldMapCell:setCreature(nil)
      newMapCell:setCreature(creatureInstance)
      
      --now move cursor
      local cursor = creatureInstance:getCursor()
      if cursor.getCell~=nil then
        cursor:getCell():setEffect(nil)
      end
      newMapCell:setEffect(cursor)
      
    
      return true
    elseif input=="menu" then
      self:getStateMachine():setState("paused")
      return true
    else
      return false
    end
  end
  
  local mapRenderer = MapRenderer.new(imageManager,nil,0,0,8,8)
  
  function instance:onDraw()
    local player = GameData:getGame():getPlayer()
    local creatureInstance=player:getInstance()
    local mapCell = creatureInstance:getCell()
    
    
    mapRenderer:setMap(mapCell:getMap())
    
    mapRenderer:render()
    
    return false
  end
  
  return instance
end

return PlayState