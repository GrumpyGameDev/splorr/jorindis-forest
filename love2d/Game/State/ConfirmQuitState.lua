local ConfirmQuitState = {}
local EventHandler = require("Handler.EventHandler")
local Menu = require("Menu.Menu")
local MenuItem = require("Menu.MenuItem")
local MenuRenderer = require("Renderer.MenuRenderer")
local RenderGrid = require("Grid.RenderGrid")
local RenderGridCellLayer = require("Grid.RenderGridCellLayer")
local GridRenderer = require("Renderer.GridRenderer")
local MenuState = require("Game.State.MenuState")

function ConfirmQuitState.new(imageManager)
  local menu = Menu.new(2,
    {MenuItem.new(" Yes ","confirm"),
     MenuItem.new(" No  ","cancel")})
  
  local instance = MenuState.new(menu)
  
  local menuRenderer = MenuRenderer.new(imageManager,menu,"ruby","black",56,56,8,8)
  local renderGrid = RenderGrid.new(20,15)
  local gridRenderer = GridRenderer.new(imageManager,renderGrid,0,0,8,8)
  
  renderGrid:fill(1,1,20,15,"empty","black","black",true)
  renderGrid:writeText(8,7,"Quit?","turquoise","black")
  
  function instance:getImageManager()
    return imageManager
  end
  
  function instance:onDraw()
    
    gridRenderer:render()
    
    menuRenderer:render()
    
    return false
  end
  
  function instance:onCommand(command)
      if command=="confirm" then
        love.event.quit()
        return true
      elseif command=="cancel" then
        self:getStateMachine():setState("mainmenu")
        return true
      else
        return false
      end
  end
  
  return instance
end


return ConfirmQuitState