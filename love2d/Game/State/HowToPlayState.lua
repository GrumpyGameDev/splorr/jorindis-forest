local HowToPlayState = {}
local EventHandler = require("Handler.EventHandler")
local Menu = require("Menu.Menu")
local MenuItem = require("Menu.MenuItem")
local MenuRenderer = require("Renderer.MenuRenderer")
local RenderGrid = require("Grid.RenderGrid")
local RenderGridCellLayer = require("Grid.RenderGridCellLayer")
local GridRenderer = require("Renderer.GridRenderer")
local MenuState = require("Game.State.MenuState")

function HowToPlayState.new(imageManager)
  local menu = Menu.new(1,
    {MenuItem.new("Back","back")})
  
  local instance = MenuState.new(menu)
  
  local menuRenderer = MenuRenderer.new(imageManager,menu,"ruby","black",64,14*8,8,8)
  local renderGrid = RenderGrid.new(20,15)
  local gridRenderer = GridRenderer.new(imageManager,renderGrid,0,0,8,8)
  
  local currentPage = 1
  local pages = 6
  
  function instance:page1()
      renderGrid:fill(1,1,20,15,"empty","black","black",true)
      renderGrid:writeText(1,1,"Controls:","turquoise","black")
      
      renderGrid:writeText(1,2,"Move: Arrows/DPad","mediumsilver","black")
      renderGrid:writeText(1,3,"Interact: E/X Button","mediumsilver","black")
      renderGrid:writeText(1,4,"Select: Mouse/LS","mediumsilver","black")
      renderGrid:writeText(1,5,"","mediumsilver","black")
      renderGrid:writeText(1,6,"","mediumsilver","black")
      renderGrid:writeText(1,7,"","mediumsilver","black")
      renderGrid:writeText(1,8,"","mediumsilver","black")
      renderGrid:writeText(1,9,"","mediumsilver","black")
      renderGrid:writeText(1,10,"","mediumsilver","black")
      renderGrid:writeText(1,11,"","mediumsilver","black")
      renderGrid:writeText(1,12,"","mediumsilver","black")
      renderGrid:writeText(1,13,"","mediumsilver","black")
      renderGrid:writeText(1,14,"","mediumsilver","black")
      
      renderGrid:writeText(1,15,"\027","silver","black")
      renderGrid:writeText(20,15,"\026","silver","black")
  end
  
  function instance:refreshPage()
    local pageName = "page"..currentPage
    if instance[pageName]~=nil then
      instance[pageName](instance)
    else
      renderGrid:fill(1,1,20,15,"empty","black","black",true)
      renderGrid:writeText(1,1,"Page "..currentPage,"turquoise","black")
      renderGrid:writeText(1,15,"\027","silver","black")
      renderGrid:writeText(20,15,"\026","silver","black")
    end
  end
  
  function instance:nextPage()
    currentPage = currentPage + 1
    if currentPage > pages then
      currentPage = 1
    end
    self:refreshPage()
  end
  
  function instance:previousPage()
    currentPage = currentPage - 1
    if currentPage < 1 then
      currentPage = pages
    end
    self:refreshPage()
  end

  instance:refreshPage()

  function instance:getImageManager()
    return imageManager
  end
  
  function instance:onDraw()
    
    gridRenderer:render()
    
    menuRenderer:render()
    
    return false
  end
  
  function instance:onCommand(command)
      if command=="back" then
        self:getStateMachine():setState("mainmenu")
        return true
      else
        return false
      end
  end
  
  local oldOnFilteredInput = instance.onFilteredInput
  
  function instance:onFilteredInput(input)
    if input=="left" then
      instance:previousPage()
      return true
    elseif input=="right" then
      instance:nextPage()
      return true
    elseif input=="cancel" then
      self:getStateMachine():setState("mainmenu")
      return true
    else
      return oldOnFilteredInput(instance,input)
    end
  end
  
  return instance
end


return HowToPlayState