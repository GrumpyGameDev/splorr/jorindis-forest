local StateHandler = {}

local MessageHandler = require("Handler.MessageHandler")

function StateHandler.new(parent,state,stateHandlers)
  local instance = MessageHandler.new(parent)
  
  local currentState = nil
  
  for id, handler in pairs(stateHandlers) do
    handler:setParent(instance)
    instance:addChild(handler)
    function handler:getStateMachine()
      return instance
    end
  end
  
  function instance:getState()
    return currentState
  end
  
  function instance:setState(newState)
    --TODO: onfinished for old state?
    currentState = newState
    for id, handler in pairs(stateHandlers) do
      if id == currentState then
        handler:enable()
      else
        handler:disable()
      end
    end
    --TODO: onstart for new state?
  end
  
  instance:setState(state)
  
  return instance
end

return StateHandler