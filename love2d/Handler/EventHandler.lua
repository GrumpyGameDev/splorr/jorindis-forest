local EventHandler = {}
local MessageId = require("Message.MessageId")
local Message = require("Message.Message")
local MessageHandler = require("Handler.MessageHandler")
local ResizeMessage = require("Message.ResizeMessage")
local KeyMessage = require("Message.KeyMessage")
local MouseMessage = require("Message.MouseMessage")
local TouchMessage = require("Message.TouchMessage")
local UpdateMessage = require("Message.UpdateMessage")
local JoystickMessage = require("Message.JoystickMessage")
local TextMessage = require("Message.TextMessage")
local FileMessage = require("Message.FileMessage")

local initializeMessageId = MessageId.next()
local gotFocusMessageId = MessageId.next()
local lostFocusMessageId = MessageId.next()
local gotMouseFocusMessageId = MessageId.next()
local lostMouseFocusMessageId = MessageId.next()
local quitMessageId = MessageId.next()
local showMessageId = MessageId.next()
local hideMessageId = MessageId.next()
local drawMessageId = MessageId.next()

function EventHandler.initializeMessageId()
  return initializeMessageId
end

function EventHandler.gotFocusMessageId()
  return gotFocusMessageId
end

function EventHandler.lostFocusMessageId()
  return lostFocusMessageId
end

function EventHandler.gotMouseFocusMessageId()
  return gotMouseFocusMessageId
end

function EventHandler.lostMouseFocusMessageId()
  return lostMouseFocusMessageId
end

function EventHandler.quitMessageId()
  return quitMessageId
end

function EventHandler.showMessageId()
  return showMessageId
end

function EventHandler.hideMessageId()
  return hideMessageId
end

function EventHandler.drawMessageId()
  return drawMessageId
end

function EventHandler.initializeMessage()
  return Message.new(EventHandler.initializeMessageId())
end

function EventHandler.gotFocusMessage()
  return Message.new(EventHandler.gotFocusMessageId())
end

function EventHandler.lostFocusMessage()
  return Message.new(EventHandler.lostFocusMessageId())
end

function EventHandler.gotMouseFocusMessage()
  return Message.new(EventHandler.gotMouseFocusMessageId())
end

function EventHandler.lostMouseFocusMessage()
  return Message.new(EventHandler.lostMouseFocusMessageId())
end

function EventHandler.quitMessage()
  return Message.new(EventHandler.quitMessageId())
end

function EventHandler.showMessage()
  return Message.new(EventHandler.showMessageId())
end

function EventHandler.quitMessage()
  return Message.new(EventHandler.hideMessageId())
end

function EventHandler.drawMessage()
  return Message.new(EventHandler.drawMessageId())
end

function EventHandler.new(parent,enabled)
  local instance = MessageHandler.new(parent)
  
  local oldOnMessage = instance.onMessage
  
  function instance:enable()
    enabled = true
  end
  
  function instance:disable()
    enabled = false
  end
  
  function instance:onInitialize()
    --print("onInitialize")
    return false
  end
  
  function instance:onGotFocus()
    --print("onGotFocus")
    return false
  end
  
  function instance:onLostFocus()
    --print("onLostFocus")
    return false
  end
  
  function instance:onGotMouseFocus()
    --print("onGotMouseFocus")
    return false
  end
  
  function instance:onLostMouseFocus()
    --print("onLostMouseFocus")
    return false
  end
  
  function instance:onQuit()
    return false
  end
  
  function instance:onShow()
    return false
  end
  
  function instance:onHide()
    return false
  end
  
  function instance:onResize(width,height)
    return false
  end
  
  function instance:onKeyPress(key,scancode,isrepeat)
    return false
  end
  
  function instance:onKeyRelease(key,scancode)
    return false
  end
  
  function instance:onMouseMove(x,y,dx,dy,istouch)
    return false
  end
  
  function instance:onMousePress(x,y,button,istouch)
    return false
  end
  
  function instance:onMouseRelease(x,y,button,istouch)
    return false
  end
  
  function instance:onMouseWheel(x,y)
    return false
  end
  
  function instance:onTouchMove(id,x,y,dx,dy,pressure)
    return false
  end
  
  function instance:onTouchPress(id,x,y,dx,dy,pressure)
    return false
  end
  
  function instance:onTouchRelease(id,x,y,dx,dy,pressure)
    return false
  end
  
  function instance:onDraw()
    return false
  end
  
  function instance:onUpdate(dt)
    return false
  end
  
  function instance:onJoystickAdd(joystick)
    return false
  end
  
  function instance:onJoystickRemove(joystick)
    return false
  end
  
  function instance:onGamepadAxis(joystick,axis,value)
    return false
  end
  
  function instance:onGamepadPress(joystick,button)
    return false
  end
  
  function instance:onGamepadRelease(joystick,button)
    return false
  end
  
  function instance:onAxis(joystick,axis,value)
    return false
  end
  
  function instance:onTextEdit(text,start,length)
    return false
  end
  
  function instance:onTextInput(text)
    return false
  end
  
  function instance:onDirectoryDrop(path)
    return false
  end
  
  function instance:onFileDrop(file)
    return false
  end
  
  function instance:onMessage(message)
    if message:getMessageId()==EventHandler.initializeMessageId() then
      return self:onInitialize()
      
    elseif not enabled then
      return oldOnMessage(self,message)
      
    elseif message:getMessageId()==EventHandler.gotFocusMessageId() then
      return self:onGotFocus()
    elseif message:getMessageId()==EventHandler.lostFocusMessageId() then
      return self:onLostFocus()
      
    elseif message:getMessageId()==EventHandler.gotMouseFocusMessageId() then
      return self:onGotMouseFocus()
    elseif message:getMessageId()==EventHandler.lostMouseFocusMessageId() then
      return self:onLostMouseFocus()
      
    elseif message:getMessageId()==EventHandler.quitMessageId() then
      return self:onQuit()
      
    elseif message:getMessageId()==EventHandler.showMessageId() then
      return self:onShow()
    elseif message:getMessageId()==EventHandler.hideMessageId() then
      return self:onHide()
      
    elseif message:getMessageId()==ResizeMessage.id() then
      return self:onResize(message:getWidth(),message:getHeight())
      
    elseif message:getMessageId()==KeyMessage.pressId() then
      return self:onKeyPress(message:getKey(), message:getScancode(), message:getRepeat())
    elseif message:getMessageId()==KeyMessage.releaseId() then
      return self:onKeyRelease(message:getKey(), message:getScancode())
      
    elseif message:getMessageId()==MouseMessage.moveId() then
      return self:onMouseMove(message:getX(),message:getY(),message:getDx(),message:getDy(),message:getTouch())
    elseif message:getMessageId()==MouseMessage.pressId() then
      return self:onMousePress(message:getX(),message:getY(),message:getButton(),message:getTouch())
    elseif message:getMessageId()==MouseMessage.releaseId() then
      return self:onMouseRelease(message:getX(),message:getY(),message:getButton(),message:getTouch())
    elseif message:getMessageId()==MouseMessage.wheelId() then
      return self:onMouseWheel(message:getX(),message:getY())
    
    elseif message:getMessageId()==TouchMessage.moveId() then
      return self:onTouchMove(message:getId(),message:getX(),message:getY(),message:getDx(),message:getDy(),message:getPressure())
    elseif message:getMessageId()==TouchMessage.pressId() then
      return self:onTouchPress(message:getId(),message:getX(),message:getY(),message:getDx(),message:getDy(),message:getPressure())
    elseif message:getMessageId()==TouchMessage.releaseId() then
      return self:onTouchRelease(message:getId(),message:getX(),message:getY(),message:getDx(),message:getDy(),message:getPressure())
      
    elseif message:getMessageId()==EventHandler.drawMessageId() then
      return self:onDraw()
    elseif message:getMessageId()==UpdateMessage.id() then
      return self:onUpdate(message:getDt())
      
    elseif message:getMessageId()==JoystickMessage.addId() then
      return self:onJoystickAdd(message:getJoystick())
    elseif message:getMessageId()==JoystickMessage.removeId() then
      return self:onJoystickRemove(message:getJoystick())
      
    elseif message:getMessageId()==JoystickMessage.gamepadAxisId() then
      return self:onGamepadAxis(message:getJoystick(),message:getAxis(),message:getValue())
    elseif message:getMessageId()==JoystickMessage.gamepadPressId() then
      return self:onGamepadPress(message:getJoystick(),message:getButton())
    elseif message:getMessageId()==JoystickMessage.gamepadReleaseId() then
      return self:onGamepadRelease(message:getJoystick(),message:getButton())
      
    elseif message:getMessageId()==JoystickMessage.axisId() then
      return self:onAxis(message:getJoystick(),message:getAxis(),message:getValue())
      
    elseif message:getMessageId()==TextMessage.editId() then
      return self:onTextEdit(message:getText(),message:getStart(),message:getLength())
    elseif message:getMessageId()==TextMessage.inputId() then
      return self:onTextInput(message:getText())
  
    elseif message:getMessageId()==FileMessage.directoryDropId() then
      return self:onDirectoryDrop(message:getPath())
    elseif message:getMessageId()==FileMessage.fileDropId() then
      return self:onFileDrop(message:getFile())
    
    else
      return oldOnMessage(self,message)
    end
  end
  
  return instance
end

return EventHandler