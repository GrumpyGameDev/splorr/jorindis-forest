local Atlas={}

local CommonMeta = require("Meta.CommonMeta")
local AtlasColumn = require("Atlas.AtlasColumn")

function Atlas.new(atlasColumns,atlasRows,mapColumns,mapRows,directions)
  local instance={}
  
  CommonMeta.makeGetter(instance,"getColumns",atlasColumns)
  CommonMeta.makeGetter(instance,"getRows",atlasRows)
  CommonMeta.makeGetter(instance,"getMapColumns",mapColumns)
  CommonMeta.makeGetter(instance,"getMapRows",mapRows)
  
  local columns = {}
  
  for column=1,atlasColumns do
    columns[#columns+1]=AtlasColumn.new(instance,column,atlasRows,mapColumns,mapRows,directions)
  end
  
  function instance:getColumn(column)
    return columns[column]
  end

  function instance:getMap(column,row)
    return self:getColumn(column):getMap(row)
  end
  
  return instance
end

return Atlas