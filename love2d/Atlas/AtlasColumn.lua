local AtlasColumn={}
local Map = require("Map.Map")

function AtlasColumn.new(atlas,atlasColumn,atlasRows,mapColumns,mapRows,directions)
  local instance = {}
  
  function instance:getAtlas()
    return atlas;
  end
  
  function instance:getColumn()
    return atlasColumn
  end
  
  local maps={}
  
  function instance:getMap(row)
    return maps[row]
  end
  
  for row=1,atlasRows do
    maps[row] = Map.new(instance,row,mapColumns,mapRows,directions)
  end  
  
  return instance
end

return AtlasColumn