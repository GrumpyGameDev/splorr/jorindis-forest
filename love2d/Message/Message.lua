local Message = {}

function Message.new(messageId)
  local instance = {}
  function instance:getMessageId()
    return messageId
  end
  return instance
end

return Message