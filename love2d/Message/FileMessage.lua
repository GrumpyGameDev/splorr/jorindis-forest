local FileMessage = {}

local MessageId = require("Message.MessageId")
local Message = require("Message.Message")

local directoryDrop = MessageId.next()
local fileDrop = MessageId.next()

function FileMessage.directoryDropId()
  return directoryDrop
end

function FileMessage.fileDropId()
  return fileDrop
end

function FileMessage.newDirectoryDrop(path)
  local instance = Message.new(FileMessage.directoryDropId())
  function instance:getPath()
    return path
  end
  return instance
end

function FileMessage.newFileDrop(file)
  local instance = Message.new(FileMessage.fileDropId())
  function instance:getFile()
    return file
  end
  return instance
end

return FileMessage