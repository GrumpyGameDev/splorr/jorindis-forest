local MouseMessage = {}
local Message = require("Message.Message")
local MessageId = require("Message.MessageId")

local mouseMoveMessageId = MessageId.next()
local mousePressMessageId = MessageId.next()
local mouseReleaseMessageId = MessageId.next()
local mouseWheelMessageId = MessageId.next()

function MouseMessage.moveId()
  return mouseMoveMessageId
end

function MouseMessage.pressId()
  return mousePressMessageId
end

function MouseMessage.releaseId()
  return mouseReleaseMessageId
end

function MouseMessage.wheelId()
  return mouseWheelMessageId
end

function MouseMessage.newMove(x, y, dx, dy, istouch)
  local instance = Message.new(MouseMessage.moveId())
  function instance:getX()
    return x
  end
  function instance:getY()
    return y
  end
  function instance:getDx()
    return dx
  end
  function instance:getDy()
    return dy
  end
  function instance:getTouch()
    return istouch
  end
  return instance
end

function MouseMessage.newPress(x, y, button, istouch)
  local instance = Message.new(MouseMessage.pressId())
  function instance:getX()
    return x
  end
  function instance:getY()
    return y
  end
  function instance:getTouch()
    return istouch
  end
  function instance:getButton()
    return button
  end
  return instance
end

function MouseMessage.newRelease(x, y, button, istouch)
  local instance = Message.new(MouseMessage.releaseId())
  function instance:getX()
    return x
  end
  function instance:getY()
    return y
  end
  function instance:getTouch()
    return istouch
  end
  function instance:getButton()
    return button
  end
  return instance
end

function MouseMessage.newWheel(x, y)
  local instance = Message.new(MouseMessage.wheelId())
  function instance:getX()
    return x
  end
  function instance:getY()
    return y
  end
  return instance
end

return MouseMessage