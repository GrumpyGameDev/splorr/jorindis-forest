local RenderGrid = {}
local RenderGridColumn = require("Grid.RenderGridColumn")
local RenderGridCellLayer = require("Grid.RenderGridCellLayer")

function RenderGrid.new(columns,rows)
  local instance = {}
  
  local gridColumns = {}
  
  for column=1,columns do
    gridColumns[#gridColumns+1]=RenderGridColumn.new(column,rows)
  end
  
  function instance:getColumns()
    return columns
  end
  
  function instance:getRows()
    return rows
  end
  
  function instance:getColumn(column)
    return gridColumns[column]
  end
  
  function instance:getCell(column,row)
    return gridColumns[column]:getCell(row)
  end
  
  function instance:writeText(column,row,text,foregroundColor,backgroundColor,overwrite)
    
    for index = 1, text:len() do
      if column>=1 and column <=self:getColumns() and row>=1 and row<=self:getRows() then
        if overwrite then
          self:getCell(column,row):clear()
        end
        
        local imageName = "character" .. text:byte(index)
        self:getCell(column,row):add(RenderGridCellLayer.new(imageName,foregroundColor,backgroundColor))
      end
      column = column + 1
    end
    
  end
  
  function instance:fill(column,row,columns,rows,imageName,foregroundColor,backgroundColor,overwrite)
    for x=column,(column+columns-1) do
      for y=row,(row+rows-1) do
        if (x>=1) and (x<=self:getColumns()) and (y>=1) and (y<=self:getRows()) then
          if overwrite then
            self:getCell(x,y):clear()
          end
          self:getCell(x,y):add(RenderGridCellLayer.new(imageName,foregroundColor,backgroundColor))
        end
      end
    end
  end
  
  
  return instance
end

return RenderGrid